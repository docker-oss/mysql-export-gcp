FROM google/cloud-sdk:alpine
WORKDIR /usr/src/app

RUN apk update
RUN apk --no-cache add mariadb-client jq tzdata

COPY dump.sh .

CMD ["sh", "dump.sh"]